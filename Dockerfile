FROM php:7.3-fpm-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1 \
    PHP_OPCACHE_VALIDATE_TIMESTAMPS ${PHP_OPCACHE_VALIDATE_TIMESTAMPS:-0} \
    PHP_OPCACHE_MAX_ACCELERATED_FILES ${PHP_OPCACHE_MAX_ACCELERATED_FILES:-10000} \
    PHP_OPCACHE_MEMORY_CONSUMPTION ${PHP_OPCACHE_MEMORY_CONSUMPTION:-192} \
    PHP_OPCACHE_MAX_WASTED_PERCENTAGE ${PHP_OPCACHE_MAX_WASTED_PERCENTAGE:-10}

RUN apk update
RUN apk add --no-cache tzdata git mc zip wget curl shadow bash vim

RUN set -ex && \
        apk add --no-cache --virtual .build-deps \
        libxml2-dev \
        shadow \
        autoconf \
        g++ \
        make \
    && apk add --no-cache imagemagick-dev imagemagick libjpeg-turbo libgomp freetype-dev \
    && pecl install imagick \
    && docker-php-ext-enable imagick \
    && apk del .build-deps

RUN docker-php-ext-install pdo_mysql mysqli mbstring

COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

COPY conf.d/php-extend.ini /usr/local/etc/php/conf.d/php-extend.ini
COPY conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

EXPOSE 9000

WORKDIR "/srv/app"
